/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pappl2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.*;  
import javax.mail.*;
import javax.mail.internet.*;  
import javax.activation.*;  



/**
 *
 * @author Luz
 */
public class Gestion {

    public Gestion() {
    }
    
   public DetteSimplifiee enregistrerRedevable(Redevable rede, ArrayList<EcheanceSimplifiee> echeancesSimp, String libelle, Double montant, String infoComplementaire, AgentComptable agent){
       DetteSimplifiee detSimpli = new DetteSimplifiee();
       detSimpli.setAgent(agent);
       LocalDateTime dateJour = LocalDateTime.now();  
       detSimpli.setDateCreation(dateJour);
       detSimpli.setEs(echeancesSimp);
       detSimpli.setInfoComplementaire(infoComplementaire);
       detSimpli.setLibelle(libelle);
       detSimpli.setMontant(montant);
       detSimpli.setRedev(rede);
       
       return detSimpli;
    }
   
    public ArrayList<DetteSimplifiee> demandeHistorique(String  nom, int annee, int moisDebut, int moisFin){
        
      ArrayList<DetteSimplifiee> historiques = new ArrayList<>();
      
      try {
        Class.forName("org.postgresql.Driver");
        
         String url = "jdbc:postgresql://localhost/PAPPL";
     
         Connection conn = DriverManager.getConnection(url,"postgres","zhang99662");
         
         String requete1 =  "SELECT redevable.nom_redevable, redevable.adresse_mail_redevable, dette.libelle, dette.montant_dette, dette.date_creation, agent_comptable.nom_agent, agent_comptable.adresse_mail_agent, dette.id_dette, "
                 + " dette.info_complementaire FROM dette JOIN agent_comptable ON (dette.adresse_mail_agent = agent_comptable.adresse_mail_agent) "
                 + "JOIN redevable ON (dette.adresse_mail_redevable = redevable.adresse_mail_redevable) "
                 + "WHERE dette.statut_dette=?";
        PreparedStatement stmt;  
        String requete2;
        if(nom != ""){
            if(annee == 0){ 
              requete2 = requete1+" AND redevable.nom_redevable =?";
              stmt = conn.prepareStatement(requete2) ;
              stmt.setBoolean(1,true);
              stmt.setString(2,nom);
            }else{
              if(moisDebut != 0){
                 requete2 = requete1 + " AND redevable.nom_redevable=? AND YEAR(dette.date_creation)=? AND MOIS(dette.date_creation)>=? AND MOIS(dette.date_creation)<=?"; 
                 stmt = conn.prepareStatement(requete2) ;
                 stmt.setBoolean(1, true);
                 stmt.setString(2,nom);
                 stmt.setInt(3, moisDebut);
                 stmt.setInt(4, moisFin);
              }else{
                 requete2 = requete1 + " AND redevable.nom_redevable=? AND YEAR(dette.date_creation)=?"; 
                 stmt = conn.prepareStatement(requete2) ;
                 stmt.setBoolean(1, true);
                 stmt.setString(2,nom);
                 stmt.setInt(3,annee);
              }      
            }
        }else{
            if(moisDebut != 0){
                 requete2 = requete1 + " AND YEAR(dette.date_creation)=? AND MOIS(dette.date_creation)>=? AND MOIS(dette.date_creation)<=?";
                 stmt = conn.prepareStatement(requete2) ;
                 stmt.setBoolean(1, true);
                 stmt.setInt(2,annee);
                 stmt.setInt(3,moisDebut);
                 stmt.setInt(4,moisFin);
              }else{
                 requete2 = requete1 + " AND YEAR(dette.date_creation)=?";  
                 stmt = conn.prepareStatement(requete2) ;
                 stmt.setBoolean(1,true);
                 stmt.setInt(2,annee);
              }      
        }
      
         ResultSet res = stmt.executeQuery();
         res.next();
        
         do{  
             AgentComptable agent = new AgentComptable();
             agent.setAdresseMail(res.getString("adresse_mail_agent"));
             agent.setNom(res.getString("nom_agent"));
             
             Redevable redevable = new Redevable();
             redevable.setAdresseMail(res.getString("adresse_mail_redevable"));
             redevable.setNom(res.getString("nom_redevable"));
             
             DetteSimplifiee detSimpli = new DetteSimplifiee();
             detSimpli.setAgent(agent);
             detSimpli.setDateCreation( res.getTimestamp("date_creation").toLocalDateTime()); 
             detSimpli.setLibelle(res.getString("libelle"));
             detSimpli.setRedev(redevable);
             detSimpli.setMontant(res.getDouble("montant_dette"));
             detSimpli.setIdDette(res.getString("id_dette"));
             historiques.add(detSimpli);
        }while (res.next()); 
            
        
         stmt.close() ;
         conn.close() ; 
        
         }
    catch (SQLException e) {
             e.printStackTrace();
    }
    catch (java.lang.ClassNotFoundException e) {
        e.printStackTrace();
    }   
       return historiques; 
    }
    
   
    
    public ArrayList<DetteSimplifiee>  demandeListeActifs (){
         ArrayList<DetteSimplifiee> actifs = new ArrayList<>();
      
      try {
        Class.forName("org.postgresql.Driver");
        
         String url = "jdbc:postgresql://localhost/PAPPL";
     
         Connection conn = DriverManager.getConnection(url,"postgres","lollol1234");
         
         String requete1 =  "SELECT redevable.nom_redevable, redevable.adresse_mail_redevable, dette.libelle,"
                 + "dette.montant_dette, dette.dette_actuelle, dette.date_creation, agent_comptable.nom_agent, agent_comptable.adresse_mail_agent, dette.id_dette,"
                 + " dette.info_complementaire FROM dette JOIN agent_comptable ON (dette.adresse_mail_agent = agent_comptable.adresse_mail_agent) "
                 + "JOIN redevable ON (dette.adresse_mail_redevable = redevable.adresse_mail_redevable) "
                 + "WHERE dette.statut_dette=?";
         
        PreparedStatement  stmt = conn.prepareStatement(requete1) ;
        stmt.setBoolean(1, false);
        ResultSet res = stmt.executeQuery();
        res.next();
        
         do{  
             AgentComptable agent = new AgentComptable();
             agent.setAdresseMail(res.getString("adresse_mail_agent"));
             agent.setNom(res.getString("nom_agent"));
             
             Redevable redevable = new Redevable();
             redevable.setAdresseMail(res.getString("adresse_mail_agent"));
             redevable.setNom(res.getString("nom_redevable"));
             
             DetteSimplifiee detSimpli = new DetteSimplifiee();
             detSimpli.setAgent(agent);
             detSimpli.setDateCreation( res.getTimestamp("date_creation").toLocalDateTime()); 
             detSimpli.setLibelle(res.getString("libelle"));
             detSimpli.setRedev(redevable);
             detSimpli.setMontant(res.getDouble("montant_dette"));
             detSimpli.setDetteActuelle(res.getDouble("dette_actuelle"));
             detSimpli.setIdDette(res.getString("id_dette"));
             actifs.add(detSimpli);
        }while (res.next()); 
            
        
         stmt.close() ;
         conn.close() ; 
        
         }
    catch (SQLException e) {
             e.printStackTrace();
    }
    catch (java.lang.ClassNotFoundException e) {
        e.printStackTrace();
    }   
       return actifs; 
    }
   
    public DetteDetaillee voirDetail(DetteSimplifiee detteS){
        DetteDetaillee detDetail = new DetteDetaillee();
       try {
           Class.forName("org.postgresql.Driver");
           
           String url = "jdbc:postgresql://localhost/PAPPL";
           
           Connection conn = DriverManager.getConnection(url,"postgres","lollol1234");
           
           String requete1 =  "SELECT redevable.nom_redevable, redevable.adresse_mail_redevable, dette.libelle, dette.montant_dette,"
                   + "agent_comptable.nom_agent, agent_comptable.adresse_mail_agent, dette.action_effectuee, dette.action_entreprendre,"
                   + "dette.info_complementaire, dette.dette_actuelle, dette.id_dette, echeance.date_deadline, echeance.montant_echeance, echeance.statut_paiement, echeance.statut_annulation,"
                   + "echeance.date_paiement, echeance.raison_annulation "
                   + "FROM dette JOIN agent_comptable ON (dette.adresse_mail_agent = agent_comptable.adresse_mail_agent) "
                   + "JOIN redevable ON (dette.adresse_mail_redevable = redevable.adresse_mail_redevable) "
                   +"JOIN echeance ON (echeance.id_dette = dette.id_dette)"
                   + "WHERE dette.id_dette=?";
           
           PreparedStatement  stmt = conn.prepareStatement(requete1) ;
           stmt.setString(1,detteS.getIdDette());
           ResultSet res = stmt.executeQuery();
           res.next();
           AgentComptable agent = new AgentComptable();
           agent.setAdresseMail(res.getString("adresse_mail_agent"));
           agent.setNom(res.getString("nom_agent"));
           Redevable redevable = new Redevable();
           redevable.setAdresseMail(res.getString("adresse_mail_agent"));
           redevable.setNom(res.getString("nom_redevable"));
           detDetail.setAgent(agent);
           detDetail.setLibelle(res.getString("libelle"));
           detDetail.setRedev(redevable);
           detDetail.setMontant(res.getDouble("montant_dette"));
           detDetail.setDetteActuelle(res.getDouble("dette_actuelle"));
           detDetail.setIdDette(res.getString("id_dette"));
           ArrayList<EcheanceDetaillee> echeanceDetails=new ArrayList<>();
           do{  
              EcheanceDetaillee echeance=new EcheanceDetaillee();
              echeance.setDateDeadLine(res.getTimestamp("date_deadline").toLocalDateTime());
              echeance.setMontant(res.getDouble("montant_echeance"));
              echeance.setStatutPaiement(res.getBoolean("statut_paiement"));
              echeance.setStatutAnnulation(res.getBoolean("statut_annulation"));
              echeance.setRaisonAnnulation(res.getString("raison_annulation"));
              Timestamp datePaiement = res.getTimestamp("date_paiement");
              if(datePaiement != null){
                echeance.setDatePaiement(datePaiement.toLocalDateTime());
              }
              echeanceDetails.add(echeance);

           }while (res.next()); 
           detDetail.setEd(echeanceDetails);
           
       }  catch (SQLException e) {
             e.printStackTrace();
    }
    catch (java.lang.ClassNotFoundException e) {
        e.printStackTrace();
    }   
       return detDetail;
    }
    
    public void enregistrerMail (String message, String jour, boolean type) throws IOException {
        
        
            String nomFichier= "InfoMail";
            File f = new File(nomFichier);
            ArrayList<String> lignes = new ArrayList();
            if (f.exists()){
                BufferedReader fichier = new BufferedReader(new FileReader(nomFichier));
                for (int i=1;i<5;i++){
                    String ligne = fichier.readLine();
                    if(ligne == null){
                        ligne = "";
                    }
                    lignes.add(ligne);
                }
            }else{
                String defautMessageR = "Vous avez une échéance a payer";
                String defautMessageA = "Un redevable n'a pas payée son échéance";
                String defautJour = "5";
                lignes.add(defautJour);
                lignes.add(defautMessageR);
                lignes.add(defautJour);
                lignes.add(defautMessageA);
                
            }
            //true pour redevable et false pour agent
            
            BufferedWriter bufferedWriter;
            bufferedWriter = new BufferedWriter(new FileWriter(nomFichier));
            if (type){
                
                bufferedWriter.write(jour);
                bufferedWriter.newLine();
                bufferedWriter.write(message);   
                bufferedWriter.newLine();
                System.out.println("ttttttttttt"+ lignes.get(3));
                bufferedWriter.write(lignes.get(2));
                bufferedWriter.newLine();
                bufferedWriter.write(lignes.get(3));
                bufferedWriter.newLine();
            }else{
               
                bufferedWriter.write(lignes.get(0));
                bufferedWriter.newLine();
                bufferedWriter.write(lignes.get(1));
                bufferedWriter.newLine();
                bufferedWriter.write(jour);
                bufferedWriter.newLine();
                bufferedWriter.write(message);
                bufferedWriter.newLine();
            }
            
            
            bufferedWriter.flush();
            bufferedWriter.close();
       
 
    }
    
    public String lireInformationMail(boolean type) throws  IOException{
            String nomFichier= "InfoMail";         
            BufferedReader fichier = new BufferedReader(new FileReader(nomFichier));
            
             String ligne;
            if(type){
                
                
                fichier.readLine();
                ligne = fichier.readLine();
                 
            }else{
                fichier.readLine();
                fichier.readLine();
                fichier.readLine();
                ligne = fichier.readLine();
           
            }
            
            return ligne; 
    }
    
    public void sendMail(){
        
    }
}
